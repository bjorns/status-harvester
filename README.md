# status-harvester

[![PyPI - Version](https://img.shields.io/pypi/v/status-harvester.svg)](https://pypi.org/project/status-harvester)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/status-harvester.svg)](https://pypi.org/project/status-harvester)

-----

**Table of Contents**

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install status-harvester
```

## License

`status-harvester` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
