import psutil
import argparse
import requests
from schedule import every, repeat, run_pending
import time
import logging
import os
logging.basicConfig(filename='general.log', encoding='utf-8', level=logging.DEBUG)
logging.debug('This message should go to the log file')

SERVER = None
SOURCE = os.uname().nodename


def send_to_server(destination, value):
    try:
        r = requests.post(f"{SERVER}/{destination}/", data={"value": str(value), "source": SOURCE})
        if str(r.status_code) != "201":
            logging.debug(f"Server did not receive the message. Status code {r.status_code}")
    except:
        logging.debug(f"Server is down.")


@repeat(every(1).minutes)
def temperature_job():
    sensors = psutil.sensors_temperatures()
    temperatures = [t.current for s in sensors.values() for t in s]
    average_temperature = sum(temperatures)/len(temperatures)
    send_to_server("cputemperature", average_temperature)


@repeat(every(1).minutes)
def processor_percent_job():
    send_to_server("cpuload", psutil.cpu_percent())


# network_status = None
# @repeat(every(5).seconds)
# def network_job():
#     global network_status
#     current_status = psutil.net_io_counters()
#     if network_status is not None:
#         bytes_received = current_status.bytes_recv - network_status['status'].bytes_recv
#         bytes_sent = current_status.bytes_sent - network_status['status'].bytes_sent
#         seconds_passed = (datetime.now() - network_status['time']).total_seconds()
#         download_speed = int(round(bytes_received / seconds_passed))
#         upload_speed = int(round(bytes_sent / seconds_passed))
#         print(f"dowload speed is {download_speed} bytes/s")
#         print(f"upload speed is {upload_speed} bytes/s")
#     network_status = {"time": datetime.now(), "status": current_status}


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            prog='status-harvester',
            description='checks a bunch of things and sends it to a server every few minutes')
    parser.add_argument('server')
    args = parser.parse_args()
    SERVER = args.server
    while True:
        run_pending()
        time.sleep(60)
